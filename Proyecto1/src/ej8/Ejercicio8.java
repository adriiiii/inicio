package ej8;

import java.util.Scanner;

public class Ejercicio8 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		// declaro dos vectores
		int[] array1 = new int[10];
		int[] array2 = new int[10];

		// relleno primer vector
		System.out.println("Rellenamos primer array");
		for (int i = 0; i < array1.length; i++) {
			System.out.println("Introduce un entero");
			array1[i] = input.nextInt();
		}

		// relleno segundo vector
		System.out.println("Rellenamos segundo array");
		for (int i = 0; i < array2.length; i++) {
			System.out.println("Introduce un entero");
			array2[i] = input.nextInt();
		}

		// llamo a sumaArray pasandole los dos vectores
		int[] arraySuma = sumarArrays(array1, array2);

		// muestro la suma
		for (int i = 0; i < arraySuma.length; i++) {
			System.out.print(arraySuma[i] + " ");
		}

		input.close();

	}

	private static int[] sumarArrays(int[] array1, int[] array2) {
		int[] sumaArray = new int[10];
		for (int i = 0; i < sumaArray.length; i++) {
			sumaArray[i] = array1[i] + array2[i];
		}
		return sumaArray;
	}

}
