package ej9;

import java.util.Scanner;

public class Ejercicio9 {

	public static void main(String[] args) {
		Scanner input=new Scanner (System.in);
		
		int[] enteros= {0,10,20,30,40,50,60,70,80,90};
		
		
		System.out.println("Introduce un entero");
		int num =input.nextInt();
		
		insertaOrdenado(enteros, num);
		for (int i=0;i<enteros.length;i++) {
			System.out.print(enteros[i]+" ");
		}
		input.close();
		
	}

	static void insertaOrdenado (int[] enteros, int num) {
		for (int i=0;i<enteros.length;i++) {
			if (enteros[i]>num) {
				enteros[i]=num;
				return;
			}
		}
		enteros[enteros.length-1]=num;
	}
	
}
